// gcc test.c -o test -g -Wall -O3 -std=gnu11 -lncurses

/*
    RULES
       There are 4 types of non-empty cells: *, 0, #, @
       .... to do ...
*/


#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>  // sleep
#include <ncurses.h> // initscr, curs_set, mvprintw, stdscr, wrefresh, endwin

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
#define ROWS (30)
#define COLS (36)
#define TRAPVAL (ROWS + COLS)

int *getNeighbors(int ptsarr[][2], int i, int j);


/***************** MAIN() *****************/
int main (void)
{
    // dramatis personnae
    const char *val2sym[5] = {" ", "0", "@", "*", "#"};
    int nMax = sizeof(val2sym)/sizeof(val2sym[0]);
    int nMin = 0;
    int arr[ROWS][COLS]; // the world array
    int narr[8][2];      // a cell's neighbors in (row, col)
    
    srand(time(NULL));

    // initialize world array
    int numbRand;
    for(int irow=0; irow<ROWS; irow++ ) {
        for(int icol=0; icol<COLS; icol++) {
            numbRand = rand()%((nMax)-nMin) + nMin;
            arr[irow][icol] = numbRand;
        }
    }

    int c = 0;
    int colSep = 1; // space between each column
    int leftEdgeSep = 1; // space from left edge of terminal window
    int topEdgeSep = 0;  // space from top edge of terminal window

    initscr (); // init ncurses mode
    curs_set (0); // hide cursor
    while (c < 100000) {
        for(int i=0; i<ROWS; i++ ) {
            for(int j=0; j<COLS; j++) {

                // use the mapping to print each value as a "being" char
                mvprintw(i+topEdgeSep, j+(colSep*j)+leftEdgeSep, "%s", val2sym[ arr[i][j] ]);

                // initialize the neighbor type counts array
                int carr[5] = {0,0,0,0,0};

                // get indices for this element's neighbors
                //printf("    (%d,%d): ", i, j);
                getNeighbors(narr, i, j);

                // check the types of these neighbors
                for(int ir=0; ir<8; ir++ ) {
                    if (narr[ir][0] != TRAPVAL)
                        //printf("(%d, %d) ", narr[ir][0], narr[ir][1]);
                        //(void)0; // no-op
                        carr[ arr[narr[ir][0]][narr[ir][1]] ] += 1;
                        
                }

                // APPLY THE RULES BELOW

                // THE CELL VALUE IS 0: EMPTPY SPACE
                if (arr[i][j] == 0) {
                    if (carr[4] >= 3) {
                        arr[i][j] = 4;
                    }
                    if (carr[0] < 2 && carr[3] >= 1 && carr[4] > 2) {
                        arr[i][j] = 3;
                    }
                }
                
                // THE CELL VALUE IS 1
                if (arr[i][j] == 1) {
                    int ncount = 0;
                    for (int ii=0; ii<8; ii++) {
                        if (narr[ii][0] != TRAPVAL) ncount += 1;
                    }
                    int randnum = rand()%(ncount);
                    // destroys this neighbor
                    arr[narr[randnum][0]][narr[randnum][1]] = 0;
                    // may destroys self
                    //if (rand()%100 == 0) arr[i][j] = 0;
                }

                // THE CELL VALUE IS 2 
                if (arr[i][j] == 2) {
                    if ( (carr[3] > 0) && (carr[4] > 0) ) {
                        for (int ii=0; ii<8; ii++) {
                            if (narr[ii][0] != TRAPVAL) {
                                arr[narr[ii][0]][narr[ii][1]] = 0;
                            }
                        }
                        if (carr[3] > 0) {
                            arr[i][j] = 3; // a @ with at least one * neighbor becomes a *
                        }                        

                    }
                }

                // THE CELL VALUE IS 3
                if (arr[i][j] == 3) {
                    if (carr[4] == 8) { // a * surrounded by # becomes a @
                        arr[i][j] = 2;
                    }
                }

                // THE CELL VALUE IS 4
                if (arr[i][j] == 4) {
                    if (carr[3] > 1) {
                        arr[i][j] = 2;
                    }
                }


            }
        }

        c += 1;
        refresh ();
        //sleep (1);
        usleep(100000);
    }
    sleep(10);
    /* End ncurses mode */
    endwin();
}


/***************** getNeighbors() *****************/
int *getNeighbors(int ptsarr[][2], int i, int j)
{
    for (int irow=0; irow<8; irow++) {
        for (int icol=0; icol<2; icol++) {
            ptsarr[irow][icol] = TRAPVAL;
        }
    }
    int count = 0;
    for(int x=MAX(0,i-1); x<=MIN(i+1, ((int)ROWS-1)); x++) {
        for(int y = MAX(0, j-1); y <= MIN(j+1, ((int)COLS-1)); y++) {
            if(x != i || y != j) {
                ptsarr[count][0] = x;
                ptsarr[count][1] = y;
                count += 1;
            }  
        }
    }
    return ptsarr;   
}
