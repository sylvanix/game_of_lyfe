![alt text][image0]

[//]: # (Image References)
[image0]: ./conflagration.gif

# Game of Lyfe  
a simulation in a 2D world comprised of cells of 4 types (excluding blank space). The interaction of neighboring cells can exhibit interesting patterns.



### About  
I thought to implement Conway's Game of Life in C, but when I had built the basic framework I wondered what would happen if I defined multiple cell types with my own rule set. Here is the result. If you run the program and the "fire" does not appear to spread, then restart as many times as necessary (usually one or two) until conditions are favorable. Eventually "flames" will touch all cells, destroying all the predatory "0" types in the process, and the "conflagration" will live on forever, or at least until the condition of the while loop is met and the program halts.

If you are interested enough to clone this repo and to compile and run, then please experiment with the rule-set. Small changes can make a great difference. If you tease out any interesting behavior, please share your ruleset with me so I can try it.

### Compiling and running

From your Linux shell, compile with gcc, ensuring that you link the ncurses library:    
```
~$ gcc test.c -o test -g -Wall -O3 -std=gnu11 -lncurses
~$ ./test
```  